import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log-viewer',
  templateUrl: './log-viewer.component.html',
  styleUrls: ['./log-viewer.component.css']
})
export class LogViewerComponent implements OnInit {
  isOn = true;
  logClicks : string[] = [];

  constructor() {
    this.doLog();
  }

  ngOnInit(): void {
  }

  doLog() {
    this.logClicks.push(new Date().toString());
  }

  toggleOnOff() {
    this.isOn = !this.isOn;
    this.doLog();
  }

  getBackgroundColor(logIndex) {
    return logIndex >= 5 ? 'blue' : 'white';
  }

  getLogClass(logIndex) {
    return logIndex >= 5 ? 'logOver5' : '';
  }
}
