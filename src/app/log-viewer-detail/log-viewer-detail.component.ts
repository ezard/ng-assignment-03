import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-log-viewer-detail',
  templateUrl: './log-viewer-detail.component.html',
  styleUrls: ['./log-viewer-detail.component.css']
})
export class LogViewerDetailComponent implements OnInit {
  @Input() logDetail;
  @Input() logIndex;

  constructor() { }

  ngOnInit(): void {
  }

}
